libtickit (0.4.2a-1) unstable; urgency=medium

  * New upstream release (Closes: #1013740)
  * Add new tickit_watch_{process,signal} symbols
  * Bump copyright years
  * Bump debhelper-compat to 13
  * Declare compliance with Policy 4.6.1, no changes needed
  * Remove version constraint for libunibilium-dev Build-Depends

 -- James McCoy <jamessan@debian.org>  Tue, 19 Jul 2022 06:34:41 -0400

libtickit (0.4.1-1) unstable; urgency=medium

  * New upstream release
  * Upload to unstable
  * Run upstream tests via autopkgtest

 -- James McCoy <jamessan@debian.org>  Sun, 04 Oct 2020 22:04:11 -0400

libtickit (0.4.0~rc4-1) experimental; urgency=medium

  * New upstream release candidate
    + Rename libtickit2 to libtickit3 for SONAME change

 -- James McCoy <jamessan@debian.org>  Sat, 23 May 2020 09:47:43 -0400

libtickit (0.3.5-1) unstable; urgency=medium

  * Update to new upstream version 0.3.5.
  * Build-Depend on debhelper-compat (= 12)

 -- James McCoy <jamessan@debian.org>  Tue, 17 Mar 2020 21:08:02 -0400

libtickit (0.3.4-1) unstable; urgency=medium

  * New upstream release
  * Rename libtickit1 to libtickit2 due to ABI bump
    + Mark all evloop symbols as optional, since there are no API/ABI guarantees
    + Remove symbols for deprecated, and now removed, tickit_string_ functions
    + Add new symbols tickit_term_build and tickit_hook_terminfo
    + Remove symbols for internal functions, and mark their counterparts as optional
    + Add symbols for tickit_version_{major,minor,patch}
    + libtickit-dev: Depend on libtickit2, not libtickit1
  * Stop installing libtickit.la
  * Declare compliance with Policy 4.5.0, no changes needed
  * autopkgtest:
    + Honor $CC when building binaries
    + Fix "undefined reference" errors
    + Add libuv1-dev/libglib2.0-dev to Depends for event loop examples
    + Add superficial restriction, since the tests are only building the
      examples.  Running them would require feeding input through a pty.

 -- James McCoy <jamessan@debian.org>  Sun, 02 Feb 2020 12:59:25 -0500

libtickit (0.2-5) unstable; urgency=medium

  * Include the man pages in libtickit-dev (Closes: #911112)
  * Add demo compilation checks for basic CI tests
  * control:
    + Add libtermkey-dev and libunibilium-dev to libtickit-dev Depends
    + Mark libtickit-dev Multi-Arch: same
  * rules:
    + Suppress build output when DEB_BUILD_OPTIONS=terse
    + Use dh_missing --list-missing
  * Declare compliance with Policy 4.2.1

 -- James McCoy <jamessan@debian.org>  Tue, 16 Oct 2018 10:48:30 -0400

libtickit (0.2-4) unstable; urgency=medium

  * Backport patch from upstream to pass CFLAGS/LDFLAGS to compiler for tests.
    This fixes a test failure in Ubuntu due to the test binaries being built
    with different fortify options than the library.  (Closes: #900034)

 -- James McCoy <jamessan@debian.org>  Thu, 31 May 2018 21:41:41 -0400

libtickit (0.2-3) unstable; urgency=medium

  * Backport patches from upstream to fix out-of-bounds access by FD_SET.
    (Closes: #895268)
  * rules:
    + Re-enable hardening since #895268 is fixed
    + Disable tests when DEB_BUILD_OPTIONS contains nocheck
  * libtickit-dev: Install examples
  * Declare compliance with Policy 4.1.4, no changes needed

 -- James McCoy <jamessan@debian.org>  Wed, 23 May 2018 22:17:24 -0400

libtickit (0.2-2) unstable; urgency=medium

  * Set TERM when running tests to fix FTBFS

 -- James McCoy <jamessan@debian.org>  Mon, 05 Feb 2018 08:06:30 -0500

libtickit (0.2-1) unstable; urgency=medium

  * Initial upload (Closes: #887312)
  * d/rules: Disable fortify flags until LP#1744933 is fixed

 -- James McCoy <jamessan@debian.org>  Wed, 31 Jan 2018 21:05:48 -0500
